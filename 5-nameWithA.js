function nameWithA(data) {
  // your code goes here

  try {
    if (typeof data !== "object") {
      throw new Error("cannot read data");
    }
    const houseData = data.houses;

    const nameWithA = houseData.reduce((accumlator, house) => {
      let peopleName = house.people;

      const peopleWithSData = peopleName
        .filter((key) => {
          let person = key.name;
          if (person.includes("a") || person.includes("A")) {
            return true;
          }

          return false;
        })
        .map((key) => key.name);

      accumlator = accumlator.concat(peopleWithSData);

      return accumlator;
    }, []);

    return nameWithA;
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = nameWithA;
