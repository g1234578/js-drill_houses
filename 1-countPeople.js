function countAllPeople(data) {
  try {
    if (typeof data !== "object") {
      throw new Error("Cannot read the data");
    }
    let houses = data.houses;
    //console.log(houses);

    let count = houses.reduce((accumlator, familyNames) => {
      accumlator += familyNames.people.length;
      return accumlator;
    }, 0);

    return count;
  } catch (error) {
    console.log(error.message);
  }
}

moduke.exports = countAllPeople;
