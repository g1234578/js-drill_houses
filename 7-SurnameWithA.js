function surnameWithA(data) {
  // your code goes here

  try {
    if (typeof data != "object") {
      throw new Error("Cannaot read the data");
    }
    const houseData = data.houses;

    const surnameWithS = houseData.reduce((accumulator, house) => {
      const people = house.people;

      people.forEach((person) => {
        const fullName = person.name;
        const splitName = fullName.split(" ");
        const lastName = splitName[splitName.length - 1]; // Assuming last part is the surname

        if (lastName.startsWith("A")) {
          accumulator.push(fullName);
        }
      });

      return accumulator;
    }, []);

    return surnameWithS;
  } catch (error) {
    console.log(error);
  }
}

module.exports = surnameWithA;
