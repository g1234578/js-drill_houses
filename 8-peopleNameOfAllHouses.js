function peopleNameOfAllHouses(data) {
  // your code goes here

  try {
    if (typeof data !== "object") {
      throw new Error("Cannot read data");
    }
    const houseData = data.houses;

    const peopleNames = houseData.reduce((accumulator, house) => {
      const people = house.people;

      const familyName = house.name;

      const namesOfPerson = people.map((key) => key.name);

      accumulator[familyName] = namesOfPerson;

      return accumulator;
    }, {});

    const sorted = Object.entries(peopleNames).sort((a, b) =>
      a[0] > b[0] ? 1 : -1
    );
    const peopleOfFamily = sorted.reduce((accumulator, familyData) => {
      const familyName = familyData[0];
      const peoples = familyData[1];
      accumulator[familyName] = peoples;
      return accumulator;
    }, {});

    return peopleOfFamily;
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = peopleNameOfAllHouses;
