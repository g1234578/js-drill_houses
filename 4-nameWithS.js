function nameWithS(data) {
  // your code goes here

  try {
    if (typeof data !== "object") {
      throw new Error("Cannot read the data");
    }
    const houseData = data.houses;

    const nameWithS = houseData.reduce((accumlator, house) => {
      let peopleName = house.people;

      const peopleWithSData = peopleName
        .filter((key) => {
          let person = key.name;
          if (person.includes("s") || person.includes("S")) {
            return true;
          }

          return false;
        })
        .map((key) => key.name);

      accumlator = accumlator.concat(peopleWithSData);

      return accumlator;
    }, []);

    return nameWithS;
  } catch (error) {
    console.log(error.message);
  }
}

module.export = nameWithS;
