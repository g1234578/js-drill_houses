function everyone(data) {
  // your code goes here

  try {
    if (typeof data !== "object") {
      throw new Error("cannot read the data");
    }
    const house = data.houses;

    let everyOneNames = house.reduce((accumalator, familyNames) => {
      let people = familyNames.people;

      let peopleName = people.map((key) => key.name);

      accumalator = accumalator.concat(peopleName);

      return accumalator;
    }, []);

    return everyOneNames;
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = everyone;
