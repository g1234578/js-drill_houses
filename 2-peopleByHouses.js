function peopleByHouses(data) {
  // your code goes here

  try {
    if (typeof data !== "object") {
      throw new Error("cannot read data");
    }
    const houses = data.houses;

    let peopleInHouses = houses.reduce((accumulator, family) => {
      accumulator[family.name] = family.people.length;

      return accumulator;
    }, {});

    let sortedData = Object.entries(peopleInHouses).sort((a, b) => {
      return a[0] > b[0] ? 1 : -1;
    });

    let countFamilyMembers = sortedData.reduce((accumulator, index) => {
      const housePeople = index[0];
      const peopleCount = index[1];
      accumulator[housePeople] = peopleCount;
      return accumulator;
    }, {});

    return countFamilyMembers;
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = peopleByHouses;
